package com.ninestack.tagged.feed

import com.ninestack.tagged.di.FragmentScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FeedModule {
    /*@FragmentScoped*/
    @ContributesAndroidInjector
    internal abstract fun feedFragment(): FeedFragment

}