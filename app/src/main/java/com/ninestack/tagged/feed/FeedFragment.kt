package com.ninestack.tagged.feed


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ninestack.tagged.R
import com.ninestack.tagged.detail.DetailActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_feed.*
import javax.inject.Inject
import android.support.design.widget.Snackbar
import android.support.design.widget.FloatingActionButton



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FeedFragment @Inject constructor() : DaggerFragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view: View = inflater?.inflate(R.layout.fragment_feed, null)!!

        val fab = view.findViewById(R.id.fab_detail) as FloatingActionButton
        fab.setOnClickListener { view ->
            startActivity(Intent(activity, DetailActivity::class.java))
        }
        return view
    }



}
