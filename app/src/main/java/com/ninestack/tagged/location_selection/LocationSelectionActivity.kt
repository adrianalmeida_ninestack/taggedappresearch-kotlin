package com.ninestack.tagged.location_selection

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ninestack.tagged.R
import dagger.android.support.DaggerAppCompatActivity

class LocationSelectionActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_selection)
    }
}
