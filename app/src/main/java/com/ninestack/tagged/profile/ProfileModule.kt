package com.ninestack.tagged.profile

import com.ninestack.tagged.di.FragmentScoped
import com.ninestack.tagged.feed.FeedFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProfileModule {

    @ContributesAndroidInjector
    internal abstract fun profileFragment(): ProfileFragment
}