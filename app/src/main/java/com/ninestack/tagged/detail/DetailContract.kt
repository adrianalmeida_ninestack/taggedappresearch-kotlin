package com.ninestack.tagged.detail

import com.ninestack.tagged.mvp.BasePresenter

class DetailContract {
    interface DetailView {
        //fun couponList(t_menuItems: List<Item>)
        fun onInsertSuccess()

        fun onInsertFailure()
    }

    interface Presenter : BasePresenter<DetailView> {
        fun insertIntoDetail(title:String,category:String,image:String)
    }
}