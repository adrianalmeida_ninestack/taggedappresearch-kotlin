package com.ninestack.tagged.detail

import android.util.Log
import com.ninestack.tagged.data.DataListener
import com.ninestack.tagged.data.DataSource
import com.ninestack.tagged.data.Repository
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class DetailActivityPresenter @Inject constructor(repository: Repository): DetailContract.Presenter {
    private var mView: DetailContract.DetailView? =null
    private var mDataSource: DataSource? = null
    private lateinit var disposable:Disposable

    init {
        mDataSource=repository
    }

    override fun insertIntoDetail(title: String, category: String, image: String) {
       /* disposable=*/ mDataSource?.insert(title, category,image, object : DataListener<DetailModel> {
            override fun onSuccess() {
                mView?.onInsertSuccess()
            }

            override fun onFailure() {
                mView?.onInsertFailure()
            }

            override fun rowId(rowId: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun tableList(list: List<DetailModel>) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun dataObject(dataObject: DetailModel) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })!!
    }

    override fun takeView(view: DetailContract.DetailView) {
        mView=view
    }

    override fun dropView() {
      /*  if (disposable!=null && disposable.isDisposed)
            disposable.dispose()*/
        mView?.let { mView = null }
    }
}