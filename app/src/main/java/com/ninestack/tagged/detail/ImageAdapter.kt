package com.ninestack.tagged.detail

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ninestack.tagged.R
import com.ninestack.tagged.utils.GlideApp
import kotlinx.android.synthetic.main.image_item.view.*

class ImageAdapter(val context: Context, var onItemClick: OnDeleteImage,var imageList:ArrayList<String>) :
    RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    interface OnDeleteImage {
        fun onDeleteClick(position: Int)
    }

    /**
     * requestCode : 1-passing initial list,2-adding items,3-removing items
     */
    fun addImageItem(image: String) {
        this.imageList?.add(image)
        notifyItemInserted(imageList.size)

    }

    fun deleteItem(position: Int) {
        this.imageList?.removeAt(position)
        notifyItemRemoved(position)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val detailImage = view.image
        val delete = view.fab_delete
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.image_item, parent, false))
    }

    override fun getItemCount(): Int {
        if (imageList != null)
            return imageList!!.size
        else
            return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //holder.detailImage.setOnClickListener()
        GlideApp.with(context)
            .load(imageList?.get(position))
            .placeholder(R.drawable.placeholder)
            .into(holder.detailImage)

        holder.delete.setOnClickListener {
            onItemClick.onDeleteClick(position)
        }
    }


}