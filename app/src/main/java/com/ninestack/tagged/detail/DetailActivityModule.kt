package com.ninestack.tagged.detail

import com.ninestack.tagged.di.FragmentScoped
import dagger.Binds
import dagger.Module

@Module
abstract class DetailActivityModule {

    @FragmentScoped
    @Binds
    internal abstract fun detailPresenter(couponPresenter: DetailActivityPresenter): DetailContract.Presenter

}