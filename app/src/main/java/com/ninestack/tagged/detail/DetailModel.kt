package com.ninestack.tagged.detail

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import io.reactivex.annotations.NonNull
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "feed_details")
@Parcelize
data class DetailModel(
    @NonNull
    @PrimaryKey(autoGenerate = true)
    var id:Int=0,
    var title: String,
  //  var imageList: ArrayList<String>,
    var category: String
) : Parcelable
