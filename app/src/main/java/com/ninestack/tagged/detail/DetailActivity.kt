package com.ninestack.tagged.detail

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import com.ninestack.tagged.R
import com.ninestack.tagged.maps.mapactivity.MapActivity
import com.ninestack.tagged.profile.ProfileFragment
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_detail.*
import javax.inject.Inject

class DetailActivity : DaggerAppCompatActivity(), View.OnClickListener, ImageAdapter.OnDeleteImage,
    DetailContract.DetailView {


    lateinit var imageAdapter: ImageAdapter
    private val GALLERY: Int = 1
    private val LOCATION=2
    private var detailModel: DetailModel? = null
    private lateinit var category: String
    private val myStrings = arrayOf("Garbage", "Haphazard Parking", "None")
    private var imageList: ArrayList<String>  = ArrayList()

    @Inject
    lateinit var detailActivityPresenter: DetailActivityPresenter
  /*  @Inject
    lateinit var mapActivity: MapActivity*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)


        //Adapter for spinner
        spinner_category.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, myStrings)
        //item selected listener for spinner
        spinner_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                category = myStrings[p2]
            }
        }

        // setup recyclerView
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        imageAdapter = ImageAdapter(this, this, arrayListOf())
        recyclerView.adapter = imageAdapter

        fab_add_image.setOnClickListener(this)
        btn_save.setOnClickListener(this)
        tvLocation.setOnClickListener(this)
        detailActivityPresenter.takeView(this)

    }

    override fun onDestroy() {
        super.onDestroy()
        detailActivityPresenter.dropView()
    }

    override fun onClick(view: View) {
        when (view.getId()) {
            R.id.fab_add_image -> {
                val photoPickerIntent = Intent(Intent.ACTION_PICK)
                photoPickerIntent.type = "image/*"
                startActivityForResult(photoPickerIntent, GALLERY)
            }
            R.id.btn_save -> {
                if (et_Title.text.toString().isEmpty() && category != null)
                    Toast.makeText(this@DetailActivity, "Please enter details", Toast.LENGTH_SHORT).show()
                else if (imageList!!.get(0) != null)
                    detailActivityPresenter.insertIntoDetail(et_Title.text.toString(), category, "")
                else
                    detailActivityPresenter.insertIntoDetail(et_Title.text.toString(), category, imageList!!.get(0))
                //finish()
            }
            R.id.tvLocation->{
                val intent=Intent(this,MapActivity::class.java)
                startActivityForResult(intent,LOCATION)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                imageList?.add(contentURI.toString())
                imageAdapter.addImageItem(contentURI.toString())
                recyclerView.scrollToPosition(imageAdapter.getItemCount() - 1);
            }
        }else if (requestCode==LOCATION){
            var lat:String= data!!.getStringExtra("latlng")
            tvLocation.text=lat
            //tvLocation.text=location
        }

    }

    override fun onDeleteClick(position: Int) {
        imageList?.removeAt(position)
        imageAdapter.deleteItem(position)
    }

    override fun onInsertSuccess() {
        Toast.makeText(this@DetailActivity, "Insert Succecs!", Toast.LENGTH_SHORT).show()
    }

    override fun onInsertFailure() {
        Toast.makeText(this@DetailActivity, "Insertion Failure", Toast.LENGTH_SHORT).show()
    }

}
