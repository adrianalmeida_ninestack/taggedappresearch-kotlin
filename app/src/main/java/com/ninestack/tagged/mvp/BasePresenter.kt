package com.ninestack.tagged.mvp

import android.view.View

interface BasePresenter<T> {
    fun takeView(view:T)
    fun dropView()
}