package com.ninestack.kotlin.di

import javax.inject.Qualifier

@Qualifier
@MustBeDocumented
@Retention annotation class Remote