package com.ninestack.tagged.di

import javax.inject.Qualifier

@Qualifier
@MustBeDocumented
@Retention annotation class ActivityScoped