package com.ninestack.tagged.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.ninestack.tagged.data.AppDatabase
import com.ninestack.tagged.utils.AppConstants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideAppDatabase(context: Context): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, AppConstants.APP_DB_NAME).build()
}

