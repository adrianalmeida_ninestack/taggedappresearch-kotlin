package com.ninestack.tagged.di.component

import android.app.Application
import com.ninestack.tagged.App
import com.ninestack.tagged.data.RepositoryModule
import com.ninestack.tagged.di.builder.ActivityBuilder
import com.ninestack.tagged.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [(AndroidInjectionModule::class), (AndroidSupportInjectionModule::class),
    (AppModule::class), (ActivityBuilder::class),(RepositoryModule::class)])
interface AppComponent {

    fun inject(app:App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

}