package com.ninestack.tagged.di.builder

import com.ninestack.tagged.DashboardActivity
import com.ninestack.tagged.detail.DetailActivity
import com.ninestack.tagged.detail.DetailActivityModule
import com.ninestack.tagged.detail.DetailActivityPresenter
import com.ninestack.tagged.feed.FeedModule
import com.ninestack.tagged.feed.FeedPresenter
import com.ninestack.tagged.location_selection.LocationSelctionPresenter
import com.ninestack.tagged.location_selection.LocationSelectionActivity
import com.ninestack.tagged.location_selection.LocationSelectionModule
import com.ninestack.tagged.maps.MapsModule
import com.ninestack.tagged.maps.MapsPresenter
import com.ninestack.tagged.maps.mapactivity.MapActivity
import com.ninestack.tagged.profile.ProfileModule
import com.ninestack.tagged.profile.ProfilePresenter
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(FeedModule::class), (MapsModule::class),(ProfileModule::class)])
    abstract fun bindMainActivity(): DashboardActivity

    @ContributesAndroidInjector(modules = arrayOf(DetailActivityModule::class))
    abstract fun bindDetailActivity():DetailActivity

    @ContributesAndroidInjector(modules = arrayOf(LocationSelectionModule::class))
    abstract fun bindLocationSelectionActivity(): LocationSelectionActivity


/*    @ContributesAndroidInjector
    abstract fun bindMapActivity(): MapActivity*/
}