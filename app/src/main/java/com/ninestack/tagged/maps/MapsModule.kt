package com.ninestack.tagged.maps

import com.ninestack.tagged.di.FragmentScoped
import com.ninestack.tagged.feed.FeedFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MapsModule {

   /* @FragmentScoped*/
    @ContributesAndroidInjector
    internal abstract fun mapFragment(): MapFragment
}