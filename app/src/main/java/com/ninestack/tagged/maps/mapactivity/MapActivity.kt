package com.ninestack.tagged.maps.mapactivity

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v4.content.res.ResourcesCompat
import android.view.View
import android.widget.Toast
import android.widget.Toolbar
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.ninestack.tagged.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_map.*
import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.util.Log
import java.io.IOException
import java.util.*
import kotlin.math.log


class MapActivity : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener {

    lateinit var gMap: GoogleMap
    private var mSelectedMarker: Marker? = null
    private var icon: BitmapDescriptor? = null
    private lateinit var latLng: LatLng

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        toolbarInitialization()

        //map initialization
        var mapFragment: SupportMapFragment? = null
        mapFragment = supportFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        tvDone.setOnClickListener(this)
    }

    fun toolbarInitialization() {
        setSupportActionBar(toolbar)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onClick(view: View) {
        when (view.getId()) {
            R.id.tvDone -> {
                var address: String = getAddress(latLng.latitude, latLng.longitude)
                Log.d("LOcation",address)
                if (address != null) {
                    val returnIntent = Intent()
                    returnIntent.putExtra("latlng",address)
                    setResult(Activity.RESULT_OK, returnIntent)
                    progressBar.visibility=View.GONE
                    finish()
                }else{
                    Toast.makeText(this@MapActivity,"Could not get location",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getAddress(latitude: Double, longitude: Double): String {
        val result = StringBuilder()
        try {
            val geocoder = Geocoder(this, Locale.getDefault())
            val addresses = geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses.size > 0) {
                val address = addresses[0]
                result.append(address.locality).append("\n")
                result.append(address.countryName)
            }
        } catch (e: IOException) {
            Log.e("tag", e.message)
        }

        return result.toString()
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        gMap = googleMap
        gMap.isMyLocationEnabled = true
        icon = getBitmapDescriptor(R.drawable.sold_selected)
        gMap.mapType = GoogleMap.MAP_TYPE_HYBRID
        gMap.uiSettings.isMapToolbarEnabled = false
        gMap.uiSettings.isZoomControlsEnabled = false
        gMap.uiSettings.isMyLocationButtonEnabled = true
        gMap.setOnMapClickListener {
            latLng = it
            if (mSelectedMarker == null)
                mSelectedMarker =
                        gMap.addMarker(MarkerOptions().position(latLng).icon(icon))/*.snippet(marker.snippet))*/
            else {
                mSelectedMarker?.remove()
                mSelectedMarker = gMap.addMarker(MarkerOptions().position(latLng).icon(icon))
                //   latLng.latitude
            }
        }

        gMap.setOnMapLoadedCallback({
            progressBar.visibility=View.GONE
        })
    }

    private fun getBitmapDescriptor(@DrawableRes id: Int): BitmapDescriptor {
        val vectorDrawable = ResourcesCompat.getDrawable(resources, id, null)
        val bitmap = Bitmap.createBitmap(
            vectorDrawable!!.intrinsicWidth,
            vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}
