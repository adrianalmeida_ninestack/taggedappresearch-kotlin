package com.ninestack.tagged.data

import com.ninestack.kotlin.di.Local
import com.ninestack.kotlin.di.Remote
import com.ninestack.tagged.data.local.LocalRepo
import com.ninestack.tagged.data.remote.RemoteRepo
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Singleton
    @Binds
    @Local
    abstract fun provideLocalDataSource(dataSource: LocalRepo): DataSource

/* @Singleton
    @Binds
    @Remote
    abstract fun provideRemoteDataSource(dataSource: RemoteRepo): DataSource*/


}
