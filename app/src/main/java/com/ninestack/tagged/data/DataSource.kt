package com.ninestack.tagged.data

import com.ninestack.tagged.detail.DetailModel
import io.reactivex.disposables.Disposable

interface DataSource {

    fun insert(title:String,category:String,image:String,repoListener: DataListener<DetailModel>): Disposable

}