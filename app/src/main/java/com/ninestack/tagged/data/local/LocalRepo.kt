package com.ninestack.tagged.data.local


import android.util.Log
import com.ninestack.tagged.data.AppDatabase
import com.ninestack.tagged.data.DataListener
import com.ninestack.tagged.data.DataSource
import com.ninestack.tagged.detail.DetailModel
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

import javax.inject.Inject
import java.nio.file.Files.delete


class LocalRepo @Inject constructor(var appDatabase: AppDatabase) : DataSource {

    private var disposable: Disposable? = null

    override fun insert(
        title: String,
        category: String,
        image: String,
        repoListener: DataListener<DetailModel>
    ): Disposable {

       /* var detailModel: DetailModel? = null
        detailModel?.id = 1
        detailModel?.title = title
        detailModel?.category = category
        appDatabase.detailDao.insertDetail(selectedCoin.getId())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(r -> {
            if(!r.isEmpty()){
                // we have some data from DB
            } else {
            }
        })*/

       /* Thread({
            var detailModel: DetailModel? = null
            //detailModel?.id = 1
            detailModel?.title = title
            detailModel?.category = category
            appDatabase.detailDao().insertDetail(detailModel!!)
        }).start()*/

        Completable.fromAction {
            var detailModel: DetailModel? = null
            detailModel?.id = 1
            detailModel?.title = title
            detailModel?.category = category
            appDatabase.detailDao().insertDetail(detailModel!!)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                    disposable = d
                }

                override fun onComplete() {
                    repoListener.onSuccess()
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    repoListener.onFailure()
                }
            })

        /* Completable.fromAction(object : Action {
             @Throws(Exception::class)
             override fun run() {
                 Log.d("Local repo class", "Running room insert query")
                 var detailModel: DetailModel? =null
                 detailModel?.id=1
                 detailModel?.title =title
                 detailModel?.category  =category
                 appDatabase.detailDao().insertDetail(detailModel!!)
             }
         }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
             .subscribe(object : CompletableObserver {
                 override fun onSubscribe(d: Disposable) {
                     disposable=d
                 }

                 override fun onComplete() {
                    repoListener.onSuccess()
                 }

                 override fun onError(e: Throwable) {
                     e.printStackTrace()
                     repoListener.onFailure()
                 }
             })*/
        return disposable!!
    }
}