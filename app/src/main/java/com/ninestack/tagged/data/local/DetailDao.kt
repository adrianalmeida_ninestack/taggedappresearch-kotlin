package com.ninestack.tagged.data.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import com.ninestack.tagged.detail.DetailModel

@Dao
interface DetailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDetail(detailModel: DetailModel)

}
