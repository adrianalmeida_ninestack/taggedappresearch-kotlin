package com.ninestack.tagged.data

import android.arch.persistence.db.SupportSQLiteOpenHelper
import android.arch.persistence.room.Database
import android.arch.persistence.room.DatabaseConfiguration
import android.arch.persistence.room.RoomDatabase
import com.ninestack.tagged.data.local.DetailDao
import com.ninestack.tagged.detail.DetailModel

@Database(entities = [(DetailModel::class)]
    ,version = 1,exportSchema = false)
abstract class AppDatabase:RoomDatabase() {

    abstract fun detailDao(): DetailDao
}
