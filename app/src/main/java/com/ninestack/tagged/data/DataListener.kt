package com.ninestack.tagged.data

interface DataListener<T> {
    fun onSuccess()
    fun onFailure()
    fun rowId(rowId: Int)
    fun tableList(list: List<T>)
    fun dataObject(dataObject: T)
}