package com.ninestack.tagged.data

import com.ninestack.kotlin.di.Local
import com.ninestack.kotlin.di.Remote
import com.ninestack.tagged.detail.DetailModel
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(@Local private var mRemoteDataSource:DataSource):DataSource{
    override fun insert(title: String, category: String,image:String, repoListener: DataListener<DetailModel>): Disposable {
       return mRemoteDataSource.insert(title,category,image,repoListener)
    }
}